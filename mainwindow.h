#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"emotivthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_8_setupdevice_clicked();

    void on_pushButton_6_link_clicked();

private:
    Ui::MainWindow *ui;
    emotivThread *emotivThread1;
};

#endif // MAINWINDOW_H
